package io.github.winx64.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class Chat extends JavaPlugin implements Listener {

	private static final String PLAYER_PLACEHOLDER = "%player%";
	private static final String MESSAGE_PLACEHOLDER = "%message%";

	private static final String CHAT_FORMAT_KEY = "chat-format";
	private static final String DEFAULT_CHAT_FORMAT = PLAYER_PLACEHOLDER + ": " + MESSAGE_PLACEHOLDER;
	private static final String CHAT_FORMAT_NOT_FOUND = "A opção '" + CHAT_FORMAT_KEY
			+ "' não esta presenta na config.yml. Usando valor padrão de '" + DEFAULT_CHAT_FORMAT + "'!";

	private String chatFormat;

	@Override
	public void onEnable() {
		this.saveDefaultConfig();
		this.getConfig().setDefaults(new YamlConfiguration());

		if (getConfig().contains(CHAT_FORMAT_KEY)) {
			this.chatFormat = getConfig().getString(CHAT_FORMAT_KEY);
		} else {
			getLogger().warning(CHAT_FORMAT_NOT_FOUND);
			this.chatFormat = DEFAULT_CHAT_FORMAT;
		}

		Bukkit.getPluginManager().registerEvents(this, this);
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		String newMessage = chatFormat.replace(PLAYER_PLACEHOLDER, event.getPlayer().getDisplayName())
				.replace(MESSAGE_PLACEHOLDER, event.getMessage());
		event.setFormat("%2$s");
		event.setMessage(ChatColor.translateAlternateColorCodes('&', newMessage));
	}
}
